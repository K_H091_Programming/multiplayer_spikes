// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "Multiplayer_Spike2_N_PlayerState.generated.h"

/**
 * 
 */
UCLASS()
class MULITPLAYER_SPIKE2_N_API AMultiplayer_Spike2_N_PlayerState : public APlayerState
{
	GENERATED_BODY()

	AMultiplayer_Spike2_N_PlayerState();

	//Replicate properties
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	//virtual void Tick(float DeltaSeconds) override;
	
public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
		int totalCoinsPS;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
		int deathsPS;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
		int winsPS;
};
