// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "Mulitplayer_Spike2_NGameMode.generated.h"

UCLASS(minimalapi)
class AMulitplayer_Spike2_NGameMode : public AGameModeBase
{
	GENERATED_BODY()

	virtual void Tick(float DeltaSeconds) override;

	//virtual void BeginPlay() override;

	//virtual void PostSeamlessTravel() override;

	void PostLogin(APlayerController* NewPlayer) override;

public:
	AMulitplayer_Spike2_NGameMode();

	UFUNCTION(BlueprintCallable, Category = "HostGame")
		void HostGame();

	UFUNCTION(BlueprintCallable, Category = "JoinGame")
		void JoinGame(FString IP);

	UFUNCTION(reliable, server, WithValidation)
		void ReadyToStartMatch();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool beginCountDown;

	void StartGameDelay(int delayTime);

	void PlayerHasDied();

	FString GetCurrentMapName();

	int myPlayers;
	int myAlivePlayers;

	bool roundFinished;

	bool loaded;

	bool spectating;

	UFUNCTION(BlueprintCallable, Category = "Spectate")
		void UnSpectate();
};
