// Fill out your copyright notice in the Description page of Project Settings.

#include "Mulitplayer_Spike2_N.h"
#include "Multiplayer_Spike2_N_PlayerState.h"
#include "UnrealNetwork.h"

//delete when all debug messages are
#include "Engine/Engine.h"

AMultiplayer_Spike2_N_PlayerState::AMultiplayer_Spike2_N_PlayerState()
{
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AMultiplayer_Spike2_N_PlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	 //Replicate to everyone
	DOREPLIFETIME(AMultiplayer_Spike2_N_PlayerState, totalCoinsPS);
	DOREPLIFETIME(AMultiplayer_Spike2_N_PlayerState, deathsPS);
	DOREPLIFETIME(AMultiplayer_Spike2_N_PlayerState, winsPS);
}

//void AMultiplayer_Spike2_N_PlayerState::Tick(float DeltaSeconds)
//{
//	Super::Tick(DeltaSeconds);
//}
