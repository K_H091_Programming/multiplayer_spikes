// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MULITPLAYER_SPIKE2_N_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
		//Replicate properties
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
	
public:

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
		FString savePlayerNameIN;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
		int totalCoinsIN;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
		int deathsIN;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated)
		int winsIN;
};
