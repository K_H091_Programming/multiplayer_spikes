// Fill out your copyright notice in the Description page of Project Settings.

#include "Mulitplayer_Spike2_N.h"
#include "MyGameInstance.h"
#include "UnrealNetwork.h"

//delete when all comments are
#include "Engine/Engine.h"


void UMyGameInstance::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(UMyGameInstance, totalCoinsIN);
	DOREPLIFETIME(UMyGameInstance, deathsIN);
	DOREPLIFETIME(UMyGameInstance, winsIN);
	DOREPLIFETIME(UMyGameInstance, savePlayerNameIN);
}