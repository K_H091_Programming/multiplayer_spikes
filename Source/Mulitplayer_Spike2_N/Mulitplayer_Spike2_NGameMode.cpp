// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Mulitplayer_Spike2_N.h"
#include "Mulitplayer_Spike2_NGameMode.h"
#include "Mulitplayer_Spike2_NPlayerController.h"
#include "Mulitplayer_Spike2_NCharacter.h"
#include "Multiplayer_Spike2_N_PlayerState.h"
#include "MySpectatorPawn.h"
#include "Runtime/ActorSequence/Private/ActorSequencePrivatePCH.h"

//Delete when debug notes are deleted
#include "Engine/Engine.h"

// TODO: Add some logic, SOMEWHERE, which does `myAlivePlayers--`

AMulitplayer_Spike2_NGameMode::AMulitplayer_Spike2_NGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMulitplayer_Spike2_NPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	
	//override the player state
	static ConstructorHelpers::FClassFinder<AMultiplayer_Spike2_N_PlayerState> NewPlayerState(TEXT("/Script/Mulitplayer_Spike2_N.Multiplayer_Spike2_N_PlayerState"));
	if (NewPlayerState.Class != NULL)
	{
		PlayerStateClass = NewPlayerState.Class;
	}

	//override the spectator pawn
	static ConstructorHelpers::FClassFinder<AMySpectatorPawn> NewSpectatorClass(TEXT("/Script/Mulitplayer_Spike2_N.MySpectatorPawn"));
	if (NewSpectatorClass.Class != NULL)
	{
		SpectatorClass = NewSpectatorClass.Class;
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	roundFinished = false;

	// TODO: Check if dedicated server - if so, this should be 0, else we will have 1 server player so...
	myPlayers = 0;
	myAlivePlayers = 0;
	loaded = false;
	spectating = false;
}

// run servers
void AMulitplayer_Spike2_NGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	// Get player details
	APlayerController* MyController = Cast<AMulitplayer_Spike2_NPlayerController>(NewPlayer);
	auto PS = Cast<AMultiplayer_Spike2_N_PlayerState>(MyController->PlayerState);

	// Get game details
	auto GI = Cast<UGameInstance>(GetGameInstance());

	TArray<AActor*> AllPlayersActive;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AMulitplayer_Spike2_NCharacter::StaticClass(), AllPlayersActive);

	// are they allowed to play? (i.e. do we have <4 current players)
	if (myPlayers < 3)
	{
		myPlayers++;
		myAlivePlayers++;
	}
	else
	{
		AMulitplayer_Spike2_NCharacter* PlayerPawn = Cast<AMulitplayer_Spike2_NCharacter>(NewPlayer->GetPawn());
		if (PlayerPawn)
		{
			PlayerPawn->QuePlayer();
			myPlayers++;
		}
	}
	
	//On logout myPlayer--, also reposess when player logs out

	// check to see if we start the game now:
	if (myPlayers >= 2)
	{
		loaded = true;
	}
}

void AMulitplayer_Spike2_NGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// UEDPIE_1_  needs to added infront of the map name to work in the editor
	if (GetCurrentMapName() == "LobbyMap") 
	{
		ReadyToStartMatch();
	}

	else if (GetCurrentMapName() == "UEDPIE_1_LobbyMap")
	{
		ReadyToStartMatch();
	}

	if (GetCurrentMapName() == "TopDownExampleMap")
	{
		if (loaded == true)
		{
			if (myAlivePlayers <= 1)
			{
				GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, TEXT("Round Won"));

				if (HasAuthority() && GetWorld())
				{
					for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
					{
						auto Character = Cast<AMulitplayer_Spike2_NCharacter>(*ActorItr);

						if (Character && roundFinished == false)
						{
							Character->CheckForWinner();
							roundFinished = true;
						}
					}
				}

				//On the client this stops the persistant data
				//Changing to seamless travel
				bUseSeamlessTravel = true;

				APlayerController* ProccessClientTravel(FString IP);

				ProcessServerTravel(FString("Level2Map"));
			}
		}
	}

	else if (GetCurrentMapName() == "UEDPIE_1_TopDownExampleMap")
	{
		if (loaded == true)
		{
			if (myAlivePlayers <= 1)
			{
				GEngine->AddOnScreenDebugMessage(1, 5.f, FColor::Red, TEXT("Round Won"));

				if (HasAuthority() && GetWorld())
				{
					for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
					{
						auto Character = Cast<AMulitplayer_Spike2_NCharacter>(*ActorItr);
						//AActor* Character = Cast<AMulitplayer_Spike2_NCharacter>(*ActorItr);

						if (Character && roundFinished == false)
						{
							Character->CheckForWinner();
							roundFinished = true;
						}
					}
				}

				//On the client this stops the persistant data
			//Changing to seamless travel
			//bUseSeamlessTravel = true;

			//APlayerController* ProccessClientTravel(FString IP);

			//ProcessServerTravel(FString("Level2Map"));
			}
		}
	}
}

void AMulitplayer_Spike2_NGameMode::HostGame()
{
	UGameplayStatics::OpenLevel(this, "LobbyMap", true, "listen");
}

void AMulitplayer_Spike2_NGameMode::JoinGame(FString IP)
{
	UGameplayStatics::OpenLevel(this, FName(*IP), true, "listen");
}

void AMulitplayer_Spike2_NGameMode::StartGameDelay(int delayTime)
{
	FTimerHandle    handle;
	if (GetWorld() && HasAuthority())
	{
		GetWorld()->GetTimerManager().SetTimer(handle, [this]() {}, delayTime, 3);
	}
}

void AMulitplayer_Spike2_NGameMode::PlayerHasDied()
{
	myAlivePlayers = myAlivePlayers--;
}

FString AMulitplayer_Spike2_NGameMode::GetCurrentMapName()
{

	UWorld* MyWorld = GetWorld();
	if (MyWorld)
		return MyWorld->GetMapName();
	else
		return FString("");
}

void AMulitplayer_Spike2_NGameMode::UnSpectate()
{
	if (spectating == true)
	{
		FConstPlayerControllerIterator ControllerIterator = GetWorld()->GetPlayerControllerIterator();

		if (ControllerIterator->IsValid())      //this->HasAuthority()
		{
			while (ControllerIterator.operator bool())
			{
				//auto PlayerStart = FindPlayerStart(CurrentController);
				AMulitplayer_Spike2_NPlayerController* CurrentController = Cast<AMulitplayer_Spike2_NPlayerController>(ControllerIterator->Get());

				if (CurrentController->IsValidLowLevel())
				{

					auto PlayerStart = FindPlayerStart(CurrentController);
					APawn* newPawn = SpawnDefaultPawnFor(CurrentController, PlayerStart);
					CurrentController->Possess(newPawn);

					++ControllerIterator;

				}
			}
		}
		spectating = false;
	}
}

void AMulitplayer_Spike2_NGameMode::ReadyToStartMatch_Implementation()
{

}

bool AMulitplayer_Spike2_NGameMode::ReadyToStartMatch_Validate()
{
	if (GetNumPlayers() >= 2)
	{
		beginCountDown = true;

		GetWorld()->ServerTravel("TopDownExampleMap");

		return true;
	}
	else
	{
		return false;
	}
}