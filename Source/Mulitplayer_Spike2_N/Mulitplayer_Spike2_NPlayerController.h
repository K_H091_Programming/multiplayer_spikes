// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/PlayerController.h"
#include "Mulitplayer_Spike2_NPlayerController.generated.h"

UCLASS()
class AMulitplayer_Spike2_NPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMulitplayer_Spike2_NPlayerController();

	void PlayerDied();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool setVariables;

	//bool spectating;

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;

	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	void Warp();

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. */
	//void SetNewMoveDestination(const FVector DestLocation);

	//Multiplayer movement edits
	UFUNCTION(reliable, server, WithValidation)
		void SetDestination(const FVector aDestination);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	UFUNCTION(BlueprintCallable, Category = "Spectate")
		void Spectate();

	//UFUNCTION(BlueprintCallable, Category = "Spectate")
	//void UnSpectate();

	int currentCoins;
	int currentHealth;

private:
	bool m_isMoveToCursor;
	FVector m_DestLocation;
};