// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Mulitplayer_Spike2_N.h"
#include "Mulitplayer_Spike2_NPlayerController.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "Mulitplayer_Spike2_NCharacter.h"
#include "MySpectatorPawn.h"

//Delete when debug notes are deleted
#include "Engine/Engine.h"
#include "Mulitplayer_Spike2_NGameMode.h"

AMulitplayer_Spike2_NPlayerController::AMulitplayer_Spike2_NPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

	setVariables = false;
}

void AMulitplayer_Spike2_NPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

		// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void AMulitplayer_Spike2_NPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AMulitplayer_Spike2_NPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &AMulitplayer_Spike2_NPlayerController::OnSetDestinationReleased);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMulitplayer_Spike2_NPlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AMulitplayer_Spike2_NPlayerController::MoveToTouchLocation);

	InputComponent->BindAction("ResetVR", IE_Pressed, this, &AMulitplayer_Spike2_NPlayerController::OnResetVR);

	InputComponent->BindAction("Warp", IE_Pressed, this, &AMulitplayer_Spike2_NPlayerController::Warp);

}

void AMulitplayer_Spike2_NPlayerController::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AMulitplayer_Spike2_NPlayerController::Warp()
{
	if (AMulitplayer_Spike2_NCharacter* MyPawn = Cast<AMulitplayer_Spike2_NCharacter>(GetPawn()))
	{
		MyPawn->Teleport();
	}
}

void AMulitplayer_Spike2_NPlayerController::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (AMulitplayer_Spike2_NCharacter* MyPawn = Cast<AMulitplayer_Spike2_NCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UNavigationSystem::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			// We hit something, move there
			SetDestination(Hit.ImpactPoint);
		}
	}
}

void AMulitplayer_Spike2_NPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetDestination(HitResult.ImpactPoint);
	}
}

//Replicating player movement
void AMulitplayer_Spike2_NPlayerController::SetDestination_Implementation(const FVector DestLocation)
{
	APawn* const Pawn = GetPawn();
	if (Pawn)
	{
		UNavigationSystem* const NavSys = UNavigationSystem::GetCurrent(this);
		float const Distance = FVector::Dist(DestLocation, Pawn->GetActorLocation());
		if (NavSys && (Distance > 120.0f))
		{
			NavSys->SimpleMoveToLocation(this, DestLocation);
		}
	}
}

bool AMulitplayer_Spike2_NPlayerController::SetDestination_Validate(const FVector DestLocation)
{
	return true;
}

void AMulitplayer_Spike2_NPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void AMulitplayer_Spike2_NPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void AMulitplayer_Spike2_NPlayerController::Spectate()
{
	auto GM = (AMulitplayer_Spike2_NGameMode*)GetWorld()->GetAuthGameMode();

	if (GM)
	{
		if (GM->spectating == false)
		{
			GM->spectating = true;

			if (AMulitplayer_Spike2_NCharacter* MyPawn = Cast<AMulitplayer_Spike2_NCharacter>(GetPawn()))
			{
				MyPawn->Destroy();
			}

			PlayerDied();
		}
	}
}

void AMulitplayer_Spike2_NPlayerController::PlayerDied()
{
	//Crashes when server dies but not when clients die
	if (this->HasAuthority())
	{
		//setVariables = true;
		 
		UnPossess();

		auto spectator = GetWorld()->SpawnActor<AMySpectatorPawn>(AMySpectatorPawn::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);

		if (spectator != nullptr)
		{
			Possess(Cast<AMulitplayer_Spike2_NCharacter>(spectator));
		}
	}
}