// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Mulitplayer_Spike2_N.h"
#include "Mulitplayer_Spike2_NCharacter.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "UnrealNetwork.h"
#include "Mulitplayer_Spike2_NPlayerController.h"
#include "Mulitplayer_Spike2_NGameMode.h"

//delete when all debug messages are deleted
#include "Engine/Engine.h"
#include "Multiplayer_Spike2_N_PlayerState.h"

AMulitplayer_Spike2_NCharacter::AMulitplayer_Spike2_NCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);

	CameraBoom->bAbsoluteRotation = false; // Don't want arm to rotate when character does
	CameraBoom->bEnableCameraRotationLag = true;
	CameraBoom->CameraRotationLagSpeed = 2;

	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

										  // Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//Set attributes to 0 at start of game
	coins = 0;
	health = 100;
	playerIsDead = false;
}

void AMulitplayer_Spike2_NCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params;
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}

	//What happens when player dies
	if (HasAuthority())
	{
		if (health <= 0 && playerIsDead == false)
		{
			playerIsDead = true;
			PlayerHasDied();
		}
	}
}

void AMulitplayer_Spike2_NCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	// Replicate to everyone
	DOREPLIFETIME(AMulitplayer_Spike2_NCharacter, health);
	DOREPLIFETIME(AMulitplayer_Spike2_NCharacter, coins);
}

void AMulitplayer_Spike2_NCharacter::CheckForWinner()
{
	if(health > 1)
	{
		if (HasAuthority())
		{
			AMultiplayer_Spike2_N_PlayerState* CurrentState = Cast<AMultiplayer_Spike2_N_PlayerState>(PlayerState);
			if (CurrentState)
			{
				CurrentState->winsPS = CurrentState->winsPS + 1;

				CurrentState->totalCoinsPS = CurrentState->totalCoinsPS + coins;
				coins = 0;
			}
		}
	}
}

void AMulitplayer_Spike2_NCharacter::QuePlayer()
{
	auto PC = Cast<AMulitplayer_Spike2_NPlayerController>(GetController());
	PC->PlayerDied();
}

void AMulitplayer_Spike2_NCharacter::TakeHealth()
{
	health = health - 20;	
}

void AMulitplayer_Spike2_NCharacter::CoinCollected(int coinsValue)
{
	coinsValue = 1;
	coins = coins + coinsValue;
}

void AMulitplayer_Spike2_NCharacter::PlayerHasDied()
{
	if (HasAuthority())
	{
		AMultiplayer_Spike2_N_PlayerState* CurrentState = Cast<AMultiplayer_Spike2_N_PlayerState>(PlayerState);
		if (CurrentState)
		{
			CurrentState->deathsPS = CurrentState->deathsPS + 1;
			CurrentState->totalCoinsPS = CurrentState->totalCoinsPS + coins;
		}
	}

	//setVariables = true;

	if (GetWorld() && HasAuthority()) 
	{
		auto GM = (AMulitplayer_Spike2_NGameMode*)GetWorld()->GetAuthGameMode();
		GM->PlayerHasDied();
	}

	// Notify the player controller that we died
	auto PC = Cast<AMulitplayer_Spike2_NPlayerController>(GetController());
	PC->PlayerDied();

	Destroy();
}

bool AMulitplayer_Spike2_NCharacter::Teleport_Validate()
{
	const FVector LineTraceStart = GetActorLocation();
	const float TraceDist = 400;
	FCollisionQueryParams QueryParams;

	FHitResult HitResult;

	const auto b_is_hit = GetWorld()->LineTraceSingleByChannel(HitResult, LineTraceStart, LineTraceStart + (GetActorForwardVector() * TraceDist), ECC_Visibility, QueryParams);

	if (b_is_hit)
	{
		// We hit something, move there
		TeleportTo(HitResult.Location, GetActorRotation());
	}
	else
	{
		TeleportTo(HitResult.TraceEnd, GetActorRotation());
	}

	return true;
}

void AMulitplayer_Spike2_NCharacter::Teleport_Implementation()
{

}